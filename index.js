/*
Goal:
	a. customer registration - done
	b. add product - done
	c. update product price - done
	d. archive product - done
	e. add to cart - done
	f. show cart - done
	g. update cart quantity - done
	h. clear cart - done
	i. compute cart total - done
	j. checkout - done
*/

/*
Requirements:
	1. Customer class: 
		a. email property - string
		b. cart property - instance of Cart class
		c. orders property - array of objects with structure {products: cart contents, totalAmount: cart total}
		d. checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty

	2. Product class:
		a. name property - string
		b. price property - number
		c. isActive property - Boolean: defaults to true
		d. archive() method - will set isActive to false if it is true to begin with
		e. updatePrice() method - replaces product price with passed in numerical value

	3. Cart class:
		a. contents property - array of objects with structure: {product: instance of Product class, quantity: number
		}	
		b. totalAmount property -number
		c. addToCart() method- accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property
		d. showCartContents() method - logs the contents property in the console
		e. updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.
		f. clearCartContents() method - empties the cart contents
		g. computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.	
*/

class Customer {
	constructor(email){
		// email property - string
		if(typeof email == 'string') {
			this.email = email;
		}
		else {
			this.email = undefined;
		}

		// instance of Cart class
		this.cart = new Cart();

		// orders property - array of objects with structure {products: cart contents, totalAmount: cart total}
		this.orders = [];
	}

	// checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty
	checkOut(){
		if(this.cart.contents.length > 0){
			this.orders.push({products: this.cart.contents ,totalAmount: this.cart.computeTotal().totalAmount})
			this.cart.contents = [];
			this.cart.totalAmount = 0;
			console.log("Computing cart total...");
			console.log("Clearing cart...");
			console.log("Order has been checkout successfully!");
		}
		else {
			console.log("Add products to the cart first!")
		}
		return this;
	}
}

class Product {
	constructor(name, price){
		// name property - string
		if(typeof name == 'string'){
			this.name = name;
		}
		else {
			this.name = undefined;
		}

		// price property - number
		if(typeof price == 'number'){
			this.price = price;
		}
		else {
			this.price = undefined;
		}

		// isActive property - Boolean: defaults to true
		this.isActive = true;
	}

	//  archive() method - will set isActive to false if it is true to begin with
	archive(){
		this.isActive = false;
		console.log(`Product ${this.name} has been removed!`);
		return this;
	}

	unarchive(){
		this.isActive = true;
		console.log(`Product ${this.name} has been added back!`);
		return this;
	}

	// updatePrice() method - replaces product price with passed in numerical value
	updatePrice(value){
		
		console.log(`${this.name}'s price changes from ${this.price} to ${value}`)
		this.price = value;
		return this;
	}
}

class Cart {
	constructor(){
		// contents property - array of objects with structure: {product: instance of Product class, quantity: number
		this.contents = [];
		// totalAmount property -number
		this.totalAmount = 0;
	}

	// addToCart() method- accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property
	addToCart(product, quantity){	
		if(product.isActive){
			this.contents.push({product,quantity})
			console.log(`Product ${product.name} has been added to the cart!`)
		}
		else {
			console.log("Product is not available!")
		}
		return this;
	}

	// showCartContents() method - logs the contents property in the console
	showCartContents(){
		if(this.contents.length > 0){
			console.log(this.contents);
		}
		else {
			console.log("Add products to the cart first!");
		}	
		return this;
	}

	// updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.
	updateProductQuantity(name, quantity){
		if(this.contents.length > 0){
			this.contents.forEach(content => {
				if(content.product.name === name){
					content.quantity = quantity;
					console.log(`The ${name}'s quantity has been changed!`)					
				}
			})
		}
		else{
			console.log("Add products to the cart first!")
		}
		
		return this;
	}

	// clearCartContents() method - empties the cart contents
	clearCartContents(){
		this.contents = [];
		this.totalAmount = 0;
		console.log("Cart contents has been cleared!")
		return this;
	}

	// computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.
	computeTotal(){
		let count = 0;
		if(this.contents.length > 0){
			this.contents.forEach(content => {
				count = count + content.quantity * content.product.price; 
			});

			this.totalAmount = count;
		}
		else{
			console.log("Add products to the cart first!")
		}
		return this;
	}

}

// Test statements
	// Customer instantiation
	const josh = new Customer("josh@mail.com")

	// Product instantiation
	const prodA = new Product('banana', 180)
	const prodB = new Product('mango',199)
	const prodC = new Product('grapes',155)
	const prodD = new Product('strawberry',165)

	// Updating price of product
	prodA.updatePrice(150)

	// Archiving product
	prodA.archive();
	// Unarchiving product
	prodA.unarchive();

	// Adding product
	josh.cart.addToCart(prodA, 3)
	josh.cart.addToCart(prodB, 4)
	josh.cart.addToCart(prodC, 2)
	josh.cart.addToCart(prodD, 1)

	// Updating cart quantity
	josh.cart.updateProductQuantity('banana',10)

	// Show cart contents
	josh.cart.showCartContents()

	// clear cart 
	josh.cart.clearCartContents()

	// Adding product to cart again
	josh.cart.addToCart(prodA, 3)
	josh.cart.addToCart(prodB, 4)
	josh.cart.addToCart(prodC, 2)
	josh.cart.addToCart(prodD, 1)

	// computing total
	josh.cart.computeTotal()

	// checkout cart contents
	josh.checkOut()
	console.log(josh)

